const items = {
    item_392019302: {
        name: "Washing Machine",
        stock: 3,
    },
    item_392019342: {
        name: "Light Bulb",
        stock: 3,
    },
    item_392019340: {
        name: "Streaming Device",
        stock: 2
    },
    item_392019389: {
        name: "Plug",
        stock: 1
    },
    item_392019311: {
        name: "Trace",
        stock: 1
    }
}


/*
Q1.Form the following solution

const result = {
    washing_machine: {
        item_id: 'item_392019302',
        stock: 3
    },
    light_bulb: {
        item_id: 'item_392019342',
        stock: 3
    },
    ...
}



Q.2 Write a function that takes items object,  propertyName and value as parameters .
The propertyName is added to each Object.
Try not to mutate the original object.

Q3. Clone items properly 
using object.assign
using spread operator.



*/
//1

const result = Object.keys(items).reduce((acc,curr) => {
    const {name,stock} = items[curr];
    acc[items[curr].name] ={item_id:curr,
             stock:stock    
            }
            return acc;
},{})
// console.log(result);

//2

const result1 = (item, key, value ) => {
    const newObject = {};
    Object.keys(item).forEach(elem => {
        newObject[elem] = {...item[elem], [key]: value}
    })
    return newObject;
};
// console.log(result1(items,'city','Bengaluru'));


//3

//clone using assign

const cloneItemUsingAssign = Object.assign({},items);
// console.log(cloneItemUsingAssign);

//clone using spread 

const cloneItemUsingSpread = {...items};
console.log(cloneItemUsingSpread);
 


